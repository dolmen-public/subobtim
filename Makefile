build:
	@cargo build
	@wasm-pack build js-suboptim


dev: build
	@echo "Open https://suboptim.docker.localhost"
	@docker-compose up --force-recreate

test:
	@cargo test
	@docker-compose run app bash -c "yarn && yarn test"

test-coverage:
	@find . -name "*.profraw" -exec rm {} \;
	@rm -rf ./coverage || true && mkdir coverage
	@RUSTFLAGS="-C instrument-coverage" CARGO_INCREMENTAL=0 LLVM_PROFILE_FILE="cargo-test-%p-%m.profraw" cargo test
	@grcov suboptim-engine --binary-path target/debug/deps -s . -t lcov --branch --ignore-not-existing --ignore '../*' --ignore "/*" -o ./coverage/tests.lcov