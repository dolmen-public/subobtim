use std::collections::HashSet;

/// A [Vector] reprensents a direction in a discrete hexagonal space.
#[derive(Debug, Clone, PartialEq, Eq, Copy, Hash)]
pub struct Vector(pub i64, pub i64);

trait CubeCoordinates {
    /// Retreives `q` axis vale in [cube coordinates](https://www.redblobgames.com/grids/hexagons/#coordinates-cube)
    fn q(&self) -> i64;

    /// Retreives `r` axis vale in [cube coordinates](https://www.redblobgames.com/grids/hexagons/#coordinates-cube)
    fn r(&self) -> i64;

    /// Retreives `s` axis vale in [cube coordinates](https://www.redblobgames.com/grids/hexagons/#coordinates-cube)
    fn s(&self) -> i64;
}

impl CubeCoordinates for Vector {
    fn q(&self) -> i64 {
        self.0
    }

    fn r(&self) -> i64 {
        self.1
    }

    fn s(&self) -> i64 {
        -self.0 - self.1
    }
}

impl Vector {
    /// Return the factored [Vector]
    pub fn multiply(&self, factor: i64) -> Self {
        Self(&self.0 * factor, &self.1 * factor)
    }

    /// Return inverted self [Vector]
    pub fn invert(&self) -> Self {
        self.multiply(-1)
    }

    /// Add [Vector] to self
    pub fn add(&self, vector: &Self) -> Self {
        Self(&self.0 + vector.0, &self.1 + vector.1)
    }

    /// Returns self length in [Hex]
    pub fn length(&self) -> i64 {
        (self.0.abs() + (self.0 + self.1).abs() + self.1.abs()) / 2
    }

    /// Rotate [Vecor] by a factor of 60°, positive factor is clock-wise
    pub fn rotate(&self, factor: u64) -> Self {
        match factor % 6 {
            6_u64..=u64::MAX | 0 => self.clone(),
            1 => Self(-self.r(), -self.s()),
            2 => Self(self.s(), self.q()),
            3 => Self(-self.q(), -self.r()),
            4 => Self(self.r(), self.s()),
            5 => Self(-self.s(), -self.q()),
        }
    }
}

/// Unitary vector to East (0°)
pub const EAST: Vector = Vector(1, 0);

/// Unitary vector to South-East (down 60°)
pub const SOUTHEAST: Vector = Vector(0, 1);

/// Unitary vector to South-West (down 120°)
pub const SOUTHWEST: Vector = Vector(-1, 1);

/// Unitary vector to West (180°)
pub const WEST: Vector = Vector(-1, 0);

/// Unitary vector to North-West (up 120°)
pub const NORTHWEST: Vector = Vector(0, -1);

/// Unitary vector to North-East (right 60°)
pub const NORTHEAST: Vector = Vector(1, -1);

#[cfg(test)]
mod vector_tests {
    use super::*;

    #[test]
    fn test_coordinates() {
        let vector = Vector(1, 2);
        assert_eq!(vector.q(), 1);
        assert_eq!(vector.r(), 2);
        assert_eq!(vector.s(), -3);
    }

    #[test]
    fn test_mulitply() {
        let vector = Vector(1, 2).multiply(3);
        assert_eq!(vector, Vector(3, 6));
    }

    #[test]
    fn test_invert() {
        assert_eq!(Vector(1, 2).invert(), Vector(-1, -2))
    }

    #[test]
    fn test_add() {
        assert_eq!(Vector(1, 2).add(&Vector(2, -2)), Vector(3, 0))
    }

    #[test]
    fn test_length() {
        assert_eq!(EAST.length(), 1);
        assert_eq!(SOUTHEAST.length(), 1);
        assert_eq!(SOUTHWEST.length(), 1);
        assert_eq!(WEST.length(), 1);
        assert_eq!(NORTHWEST.length(), 1);
        assert_eq!(NORTHEAST.length(), 1);

        assert_eq!(Vector(1, 1).length(), 2);
        assert_eq!(Vector(2, 2).length(), 4);
    }

    #[test]
    fn test_rotate() {
        let vector = Vector(5, -2);
        assert_eq!(vector.rotate(1), Vector(2, 3));
        assert_eq!(Vector(2, 1).rotate(1), Vector(-1, 3));
        assert_eq!(vector.rotate(2), Vector(-3, 5));
        assert_eq!(vector.rotate(3), Vector(-5, 2));
        assert_eq!(vector.rotate(4), Vector(-2, -3));
        assert_eq!(vector.rotate(5), Vector(3, -5));
        assert_eq!(vector.rotate(6), vector);
        assert_eq!(vector.rotate(32), Vector(-3, 5));

        assert_eq!(EAST.rotate(1), SOUTHEAST);
        assert_eq!(EAST.rotate(2), SOUTHWEST);
        assert_eq!(EAST.rotate(3), WEST);
        assert_eq!(EAST.rotate(4), NORTHWEST);
        assert_eq!(EAST.rotate(5), NORTHEAST);
        assert_eq!(EAST.rotate(6), EAST);
    }

    #[test]
    fn test_from() {
        let vector = Vector(1, 2);
        let hex = Hex(1, 2);
        assert_eq!(Vector::from(hex), vector);
        assert_eq!(Hex::from(vector), hex);
    }
}

/// A [Hex] is a cell in a discrete hexagonal space. See [more](https://www.redblobgames.com/grids/hexagons/#coordinates-cube)
#[doc=include_str!("cube_coordinates.svg")]
#[derive(Debug, Clone, PartialEq, Eq, Copy, Hash)]
pub struct Hex(pub i64, pub i64);

impl Hex {
    /// Computes [Vector] from self to given [Hex]
    pub fn vector_to(&self, to: &Self) -> Vector {
        Vector(to.0 - self.0, to.1 - self.1)
    }

    /// Computes [Vector] to self from given [Hex]
    pub fn vector_from(&self, from: &Self) -> Vector {
        from.vector_to(self)
    }

    /// Add given [Vector] to self and return the resulting [Hex]
    pub fn add(&self, vector: &Vector) -> Self {
        Self(&self.0 + vector.0, &self.1 + vector.1)
    }

    /// Rotate [Hex] by a factor of 60°, positive factor is clock-wise
    pub fn rotate(&self, center: &Hex, factor: u64) -> Self {
        center.add(&self.vector_from(center).rotate(factor))
    }

    /// Return a [HashSet] of [Hex] from the ring sized `n` around self
    pub fn ring(&self, size: u32) -> HashSet<Hex> {
        let mut hexes: HashSet<Hex> = HashSet::new();
        if size == 0 {
            hexes.insert(self.clone());
            return hexes;
        }

        let mut ring_side_hexes = Vec::<Hex>::new();

        let mut current_hex = self.add(&EAST.multiply(size.into()));

        for _ in 1..size + 1 {
            hexes.insert(current_hex.clone());
            ring_side_hexes.push(current_hex.clone());
            current_hex = current_hex.add(&NORTHWEST);
        }

        for rotation_factor in 1..6 {
            for hex in ring_side_hexes.clone() {
                hexes.insert(hex.rotate(self, rotation_factor));
            }
        }

        return hexes;
    }
}

impl From<Hex> for Vector {
    fn from(value: Hex) -> Self {
        Vector(value.0, value.1)
    }
}

impl From<Vector> for Hex {
    fn from(value: Vector) -> Self {
        Hex(value.0, value.1)
    }
}

#[cfg(test)]
mod hex_tests {
    use super::*;

    #[test]
    fn test_vector_to() {
        assert_eq!(Hex(3, 2).vector_to(&Hex(2, -5)), Vector(-1, -7))
    }

    #[test]
    fn test_vector_from() {
        assert_eq!(Hex(3, 2).vector_from(&Hex(2, -5)), Vector(1, 7))
    }

    #[test]
    fn test_add() {
        assert_eq!(Hex(3, 2).add(&Vector(-1, -7)), Hex(2, -5))
    }

    #[test]
    fn test_rotate() {
        assert_eq!(Hex(3, 2).rotate(&Hex(1, 1), 1), Hex(0, 4));
        assert_eq!(Hex(2, -4).rotate(&Hex(0, 0), 4), Hex(-4, 2));
    }

    #[test]
    fn test_ring() {
        assert_eq!(Hex(5, 7).ring(0), HashSet::from([Hex(5, 7)]));

        assert_eq!(
            Hex(0, 0).ring(2),
            HashSet::from([
                Hex(2, -2),
                Hex(1, -2),
                Hex(0, -2),
                Hex(-1, -1),
                Hex(-2, 0),
                Hex(-2, 1),
                Hex(-2, 2),
                Hex(-1, 2),
                Hex(0, 2),
                Hex(1, 1),
                Hex(2, 0),
                Hex(2, -1)
            ])
        );

        assert_eq!(Hex(3, 4).ring(5).len(), 30)
    }
}
