import { Period } from "js-suboptim/js_suboptim";
import { describe, it, expect } from "vitest";

describe("period", async () => {
  it("creates a period", () => {
    const date = new Date("2023-01-01T01:02:03+02:00");
    const date2 = new Date("2023-01-02T01:02:03Z");
    const period = new Period(date, date2);

    expect(period.start).toBe("2022-12-31T23:02:03+00:00");
    expect(period.end).toBe("2023-01-02T01:02:03+00:00");
  });

  it("gets period between 2", () => {
    const period1 = Period.from_iso(
      "2023-01-02T01:02:03+00:00",
      "2023-01-04T01:02:03Z"
    );
    const period2 = Period.from_iso(
      "2023-02-02T01:02:03Z",
      "2023-02-04T01:02:03Z"
    );
    const period = period1.between(period2);

    expect(period?.start).toBe("2023-01-04T01:02:03+00:00");
    expect(period?.end).toBe("2023-02-02T01:02:03+00:00");
  });
});
